from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def home(request,param=None):
    if param :
        return HttpResponse('bonjour inconnu '+param)
    else : 
        return HttpResponse('bonjour à tous')
